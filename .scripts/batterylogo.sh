#!/bin/bash
value=$(acpi | grep Battery | cut -d " " -f4 | tr -d "%,")

if [ $value == '100' ]; then
	echo 
elif [ $value -lt '100' ] && [ $value -ge '75' ]; then
	echo 
elif [ $value -lt '80' ] && [ $value -ge '50' ]; then
	echo 
elif [ $value -lt '60' ] && [ $value -ge '35' ]; then
	echo 
elif [ $value -lt '40' ] && [ $value -ge '10' ]; then
	echo 
elif [ $value -lt '20' ] && [ $value -ge '10' ]; then
	echo 
fi
