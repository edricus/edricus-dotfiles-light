# edricus-dotfiles-light
Light version of my dotfiles. Less information on the bar

![alt text](http://i.imgur.com/93PQIxJ.jpg)
![alt text](http://i.imgur.com/zexmYct.jpg)
![alt text](https://i.imgur.com/yj14sOS.jpg)

<h2> Quick presentation </h2>
WM : i3-gaps<br />
Fonts : system-san-francisco-font-git, font awesome for icons<br />  
Bar : i3bar with conky<br />

<h2> Dependencies </h2>
conky<br />
font awesome<br />
acpi<br />
jq<br />
files in .scripts folder must be executable (chmod +x .scripts/*)  
